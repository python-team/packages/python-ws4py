Source: python-ws4py
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Alexandre Detiste <tchet@debian.org>,
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3-all,
               python3-cherrypy3,
# Workaround for https://bugs.debian.org/1027751.
               python3-exceptiongroup,
               python3-pytest,
               python3-setuptools,
               python3-sphinx,
               python3-sphinxcontrib.seqdiag,
               python3-tornado,
Rules-Requires-Root: no
Build-Depends-Indep: python3-doc
Standards-Version: 4.6.2
Homepage: https://ws4py.readthedocs.org/
Vcs-Git: https://salsa.debian.org/python-team/packages/python-ws4py.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-ws4py
Testsuite: autopkgtest-pkg-python

Package: python3-ws4py
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends},
         ${sphinxdoc:Depends},
Suggests: python3-cherrypy3 | python3-tornado,
          python-ws4py-doc
Description: WebSocket library
 Python library providing an implementation of the WebSocket protocol defined
 in RFC 6455.

Package: python-ws4py-doc
Architecture: all
Section: doc
Depends: ${misc:Depends},
         ${sphinxdoc:Depends},
Replaces: python-ws4py (<< 0.4.2+dfsg1-1),
          python3-ws4py (<< 0.4.2+dfsg1-1),
Breaks: python-ws4py (<< 0.4.2+dfsg1-1),
        python3-ws4py (<< 0.4.2+dfsg1-1),
Multi-Arch: foreign
Description: WebSocket library (docs)
 Python library providing an implementation of the WebSocket protocol defined
 in RFC 6455.
 .
 This package contains documentation.
